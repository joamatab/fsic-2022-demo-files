
depth(5.0)
height(5.0)

layers_file("xs.lyp")

trench = 3.0              # trench depth

l1 = layer("1/0")

sub = bulk

mask(l1).etch(trench, 0.0, into: sub)

sheet1 = deposit(0.5, 1.0, mode: :round)
sheet2 = mask(l1).grow(1.0, 0.55, mode: :round)
sheet3 = deposit(0.5, 0.5, mode: :round)
sheet4 = deposit(2.0, 2.0, mode: :round)

output("1/0", sub)
output("2/0", sheet1)
output("3/0", sheet2)
output("4/0", sheet3)
output("5/0", sheet4)


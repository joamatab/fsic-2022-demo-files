
depth(5.0)
height(5.0)
layers_file("xs.lyp")

trench = 3.0         # trench depth

l1 = layer("1/0")

sub = bulk

# sort the input shapes in bins so we can apply different shapes

l1_gt1 = l1.sized(-0.5).sized(0.5)        # l1 shapes > 1.0um
l1_bin1 = l1.not(l1_gt1)                  # l1 shapes <= 1.0um
l1_gt2 = l1_gt1.sized(-1.0).sized(1.0)    # l1 shapes > 2.0um
l1_bin2 = l1_gt1.not(l1_gt2)              # l1 shapes <= 2.0um > 1.0um
l1_gt3 = l1_gt2.sized(-1.5).sized(1.5)    # l1 shapes > 3.0um
l1_bin3 = l1_gt2.not(l1_gt3)              # l1 shapes <= 3.0um > 2.0um

mask(l1).etch(trench, 0.0, into: sub)

# bin 1 (<= 1.0um) gets 0.2um bottom and a taper angle of 4 degree
# Use a small seed on 10nm left and right of the trench and at the top
# edge. Produce a sidewall by depositing with the trench depth.
#
sheet = mask(l1_bin1).grow(0.2, 0.0)
sheet = sheet.or(mask(l1_bin1.sized(0.01).not(l1_bin1)).grow(trench, 0.4, taper: 4.0))

# bin 2 (<= 2.0um > 1.0um) gets 0.5um bottom and a taper angle of 2 degree
# See before for the scheme

sheet = sheet.or(mask(l1_bin2).grow(0.5, 0.0))
sheet = sheet.or(mask(l1_bin2.sized(0.01).not(l1_bin2)).grow(trench, 0.4, taper: 2.0))

# bin 3 (<= 2.0um > 1.0um) gets 0.5um bottom and no taper
# See before for the scheme

sheet = sheet.or(mask(l1_bin3).grow(0.8, 0.0))
sheet = sheet.or(mask(l1_bin3.sized(0.01).not(l1_bin3)).grow(trench, 0.4, taper: 0.0))

# Remove the donkey ears that appear due to deposition which also goes upwards

planarize(downto: sub, into: sheet)

# Produce a top layer
# Note that by using buried -0.2 and a grow value of 0.2 (up and down) we
# get a 0.4um sheet only above the top edge. After that we can safely apply the 
# rounded part above.

top_sheet = mask(l1.inverted).grow(0.2, 0.4, buried: -0.2) 
top_sheet = top_sheet.or(mask(l1.inverted).grow(0.4, 0.4, mode: :round))

sheet = sheet.or(top_sheet)

# Output section

output("1/0", sub)
output("2/0", sheet)


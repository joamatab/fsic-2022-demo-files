
depth(5.0)
layers_file("xs.lyp")

trench = 3.0         # trench depth
sheet_bottom = 0.4   # sheet thickness
sheet_top = 0.8      # sheet thickness

l1 = layer("1/0")

sub = bulk

# Idea:
# First deposit a thinner top sheet layer which we will etch
# away so the bottom does not see it.
# After that we deposit a homogenous sheet adding to the 
# one left at the top.

sheet = deposit(sheet_top - sheet_bottom)

mask(l1).etch(trench + sheet_bottom, 0.0, into: [sub, sheet])

sheet = sheet.or(deposit(sheet_bottom, sheet_bottom, mode: :round))

output("1/0", sub)
output("2/0", sheet)

